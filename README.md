# docker-dev

A Dockerfile with the essentials for C/C++ development

## What's in it?

The Dockerfile defines two distinct images:
1. `dev-base` - basic development environment with all that's needed to compile
and test C/C++ code.
2. `yocto` - Based on `dev-base`, plus all the essentials for building yocto.

The docker image is based on `ubuntu 18.04`, and you have the following tools installed:

1. `git`
2. `make`
3. `gcc` compiler suite
4. `clang` compiler suite
5. `CMake`
6. `pkg-config`
7. `conda`

In addition there are some useful configuration files (bashrc, zshrc, inputrc)
so that the shell experience won't suck.

### User
The images have a regular user `dev` in addition to `root`.
`dev` has 1000:1000 UID:GID so that it'll map naturally to the default user on
most distros.

## Usage

You should use the official images uploaded to [shukron/dev-base](https://cloud.docker.com/repository/docker/shukron/dev-base) and [shukron/yocto](https://cloud.docker.com/repository/docker/shukron/yocto):

```bash
docker pull shukron/dev-base:latest
docker pull shukron/yocto:latest
```

Alternatively, you can build the image yourself:

```bash
git clone https://gitlab.com/shukron/docker-dev.git
cd docker-dev
git submodule update --init --recursive
docker build .
```

You can then use the image to compile and test your code.
See the official docker documentation for more details.
