FROM ubuntu:18.04 AS dev-base

ENV USER=dev
ENV HOME=/home/${USER}

ARG PUID=1000
ARG PGID=1000

RUN apt-get clean -y
RUN apt-get update && apt-get upgrade --fix-missing -y

# Install common essential tools
RUN apt-get install -y git wget curl neovim \
                        python3-neovim nano \
                        build-essential locales \
                        sudo zsh

# Install a locale that supports agnoster theme fonts
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8

RUN groupadd -g ${PGID} ${USER} \
        && useradd -u ${PUID} -g ${USER} -G sudo -d ${HOME} ${USER} -s $(which zsh) \
        && mkdir ${HOME} \
        && chown -R ${USER}:${USER} ${HOME}

# Allow members of the sudo group to run sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER ${USER}
WORKDIR ${HOME}

# Install oh-my-zsh
RUN sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
CMD ["zsh"]

# Install miniconda
RUN wget -O ${HOME}/miniconda3.sh https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
RUN chmod u+x ${HOME}/miniconda3.sh
RUN ${HOME}/miniconda3.sh -b -p ${HOME}/miniconda3
RUN rm ${HOME}/miniconda3.sh

# Add configs from home/.
COPY --chown=dev:dev home/.*rc \
                home/.bash_aliases \
                home/.gitconfig \
                home/.tmux.conf ${HOME}/
COPY --chown=dev:dev home/nvim ${HOME}/.config/nvim/
RUN mkdir -p ${HOME}/.local/share/nvim/site/spell
RUN wget -O ${HOME}/.local/share/nvim/site/spell/en.utf-8.spl http://ftp.vim.org/pub/vim/runtime/spell/en.utf-8.spl
# Install neovim plugins. Returns an error, but succeeds.
RUN nvim -i NONE -c PlugInstall -c quitall > /dev/null 2>&1 || true

# Initialize conda in bash and zsh
RUN ${HOME}/miniconda3/bin/conda init bash zsh
RUN ${HOME}/miniconda3/bin/conda update -n base -c defaults conda

ADD base-env.txt ${HOME}/
RUN ${HOME}/miniconda3/bin/conda install -n base --file ${HOME}/base-env.txt && rm ${HOME}/base-env.txt
RUN ${HOME}/miniconda3/bin/conda clean -a -y

FROM dev-base AS yocto
# Install essential requirements of the Yocto project
RUN sudo apt-get install -y diffstat gcc-multilib socat gawk wget unzip \
        texinfo chrpath socat cpio python3-distutils

USER ${USER}
